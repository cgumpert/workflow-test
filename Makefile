all: hello

hello: main.cxx
	g++ -o $@ $^

.PHONY: run
run: hello
	./hello
