#include <iostream>

int main()
{
  std::string name;
  std::cout << "What's your name? ";
  std::cin >> name;
  
  std::cout << "Hello " << name << ", I am happy to see you!" << std::endl;

  bool bBadWeather = false;
  std::cout << "Is it raining? ";
  std::cin >> bBadWeather;

  if(bBadWeather)
    std::cout << "Stay in bed!" << std::endl;
  else
    std::cout << "Have a nice day!" << std::endl;

  std::cout << "Hope to see you soon, buddy!" << std::endl;
  
  
  
  return 0;
}
